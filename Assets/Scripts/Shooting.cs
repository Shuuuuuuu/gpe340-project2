﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    //Variables
    Enemy enemyHealth;
    public GameObject bullet;
    public Transform spawnPoint;
    public float speed = 100f;

    //Calls in for the Enemy Script
    void Start()
    {
        enemyHealth = FindObjectOfType<Enemy>();
    }

    //When we press space we start shooting
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject instBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            instBullet.GetComponent<Rigidbody>().AddRelativeForce(spawnPoint.forward * speed);
            Destroy(instBullet, 3.0f);
        }
    }
}
