﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //Call Pawn from Pawn Script
    [Header("Calls in the Pawn Script.")]
    [SerializeField]
    private Pawn pawn;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Controlling our character with Horizontal and Vertical Inputs
        pawn.Move(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")));
        
    }
}
