﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    //Inputs
    public float maxHealth = 100f;
    public float damage = 10f;
    public Slider slider;
    public Image fillImage;
    public Color fullHpColor = Color.cyan;
    public Color zeroHpColor = Color.red;
    [SerializeField]
    public float curHealth;

    //Updating
    public void Update()
    {
        SetHpUI();
        OnDeath();
    }

    //Lets the character enable its HP and Sets up the UI
    public void OnEnable()
    {
        curHealth = maxHealth;
        SetHpUI();
    }

    //When taken damage its hp will be lower
    public void Damage(float damage)
    {
        curHealth -= damage;
        SetHpUI();
    }

    //Set up how the UI for the hp will look like
    public void SetHpUI()
    {
        slider.value = curHealth;

        fillImage.color = Color.Lerp(zeroHpColor, fullHpColor, curHealth / maxHealth);
    }

    //When Hp is 0 destroy gameObject
    public void OnDeath()
    {
        if (curHealth <= 0f)
        {
            gameObject.SetActive(false);
        }
    }

    //When it collides with bullet gets damage
    public void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag == "Bullet")
        {
            curHealth -= 10;
        }
    }

    //This Resets the health and the UI when killed
    public void Reset()
    {
        curHealth = maxHealth;
        SetHpUI();
    }
}
