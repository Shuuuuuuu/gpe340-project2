﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickUp : MonoBehaviour
{
    //Input
    public GameObject myWeapon;
    public GameObject weaponOnGround;
   

    public void Start()
    {
        
    }

    private void Update()
    {
        
    }


    //Triggres with character it will destroy the object and equip
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            myWeapon.SetActive(true);
            weaponOnGround.SetActive(false);
        }
    }
}
