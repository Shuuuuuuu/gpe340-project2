﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    //Variables
    public GameObject enemyPrefab;
    public Transform spawnPoint;
    List<GameObject> enemies = new List<GameObject>();

    //This will Spawn our enemies
    private void Start()
    {
        SpawnEnemy();
    }

    //This will invoke our adding of enemies and where
    public void SpawnEnemy()
    {
       GameObject enemy = Instantiate(enemyPrefab);
       enemy.transform.position = new Vector3(8, 0, -16);
       AddEnemy(enemy);
        Invoke("SpawnEnemy", 5f);
    }

    //This will add the enemies
    public void AddEnemy(GameObject enemy)
    {
        enemies.Add(enemy);
    }
}
