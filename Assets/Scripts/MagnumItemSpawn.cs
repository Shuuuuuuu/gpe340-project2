﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnumItemSpawn : MonoBehaviour
{
    //Input
    private float degreesPerSecond = 15f;
    private float amplitude = 0.5f;
    private float frequency = 1;

    //Position Variables
    Vector3 posOffSet = new Vector3();
    Vector3 tempPos = new Vector3();

    private void Start()
    {
        posOffSet = transform.position;
    }

    private void Update()
    {
        //Spin Around
        transform.Rotate(new Vector3(0f, Time.deltaTime * degreesPerSecond, 0f), Space.World);

        //Float
        tempPos = posOffSet;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
        transform.position = tempPos;
    }

    //Triggres with character it will destroy the object and equip
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Destroy(gameObject);
        }
    }

}
