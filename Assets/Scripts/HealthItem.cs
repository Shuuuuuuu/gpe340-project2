﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthItem : MonoBehaviour
{
    //Inputs
    Pawn playerHealth;
    public float healthBonus = 10f;
    private float destroyTime = 5f;
    private float degreesPerSecond = 15f;
    private float amplitude = 0.5f;
    private float frequency = 1;

    //Position Variables
    Vector3 posOffSet = new Vector3();
    Vector3 tempPos = new Vector3();

    //OnStart functions
    private void Start()
    {
        playerHealth = FindObjectOfType<Pawn>();
        posOffSet = transform.position;
        Destroy(gameObject, destroyTime);
    }

    //Updates our item to float and rotate
    private void Update()
    {
        //Spin around Y-Axis
        transform.Rotate(new Vector3(0f, Time.deltaTime * degreesPerSecond, 0f), Space.World);

        //Float up and down
        tempPos = posOffSet;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
        transform.position = tempPos;
    }

    //When it triggres againts our character will add to its health and destroy this object.
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            playerHealth.curHealth += 10;
        }
    }
}
