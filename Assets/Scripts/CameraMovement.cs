﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    // Pass the transform to the characters rotation
    public Transform tf;
    public float targetRotateSpeed;
    public float smoothTime = 0.3f;
    private Vector3 velocity = Vector3.zero;
    private Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        RotateOnMouse();
        Vector3 goalPos = tf.position;
        goalPos.y = transform.position.y;
        transform.position = Vector3.SmoothDamp(transform.position, goalPos, ref velocity, smoothTime);
    }

    public void RotateOnMouse()
    {
        // The plane where our character is
        Plane groundPlane;
        // Getting the worlds up
        groundPlane = new Plane(Vector3.up, tf.position);

        // Raycast from the camera to the mouse
        float distance;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        // Groundplane Ray hits out point
        if (groundPlane.Raycast(ray, out distance))
        {
            //Finds where the world point intersection
            Vector3 intersectionPoint = ray.GetPoint(distance);
            // Character rotate towards the point
            Quaternion targetRot;
            Vector3 lookVector = intersectionPoint - tf.position;
            targetRot = Quaternion.LookRotation(lookVector, Vector3.up);
            tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRot, targetRotateSpeed * Time.deltaTime);
        }
    }

   
}
